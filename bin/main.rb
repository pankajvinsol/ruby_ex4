# Public: Check whether given string is a palindrom or not,
# To Quit Application press q or Q
#
# Examples
#
#   ruby main.rb
#   123321
#   => It is a palindrome
#   123456
#   => It is not a palindrome
#   q
#
# Print whether input string is a palindrome or not

require_relative "../lib/string.rb"

while line = gets
  if line =~ /^q$/i
    break
  elsif line.palindrome?
    puts "It is a palindrome"
  else
    puts "It is not a palindrome"
  end
end

