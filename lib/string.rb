# Public: Extended String Class to check whether string is a palindrome or not
#
# Examples
#
#   # "lol".palindrome?
#   # true
#   # "ruby".palindrome?
#   # false
#
# return true or false

class String

  def palindrome?
    chomp.eql? chomp.reverse
  end

end

